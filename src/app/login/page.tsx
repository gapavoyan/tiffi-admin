"use client";

import Image from "next/image";
import React from "react";
import { useRouter } from "next/navigation";
import { Controller, useForm } from "react-hook-form";
import TextFiled from "../../../components/login-input/input";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import Api from "../../../api";
import { UseLogin } from "../../../hooks/auth/use-login";

export interface LoginFormValues {
  email: string;
  password: string;
}

const schema = z.object({
  email: z
    .string()
    .min(1, { message: "Требуется электронная почта" })
    .email({ message: "Неверный формат электронной почты" }),
  password: z
    .string()
    .min(6, { message: "Пароль должен быть не менее 6 символов" })
    .max(100, { message: "Пароль должен содержать менее 100 символов." }),
});

function Login() {
  const { push } = useRouter();
  const { addToken } = UseLogin();
  const { control, handleSubmit, setError } = useForm<LoginFormValues>({
    resolver: zodResolver(schema),
  });

  async function onSubmit(info: LoginFormValues) {
    console.log(info, "info");

    const { data, meta } = await Api.auth.PostLogin(info);
    console.log(data);

    if (meta.error) {
      setError("password", {
        message: "Неправильный логин или пароль",
      });
    } else {
      addToken(data?.token);
      push("/");
    }
  }

  return (
    <div className="h-screen w-full bg-green flex justify-center items-center">
      <div className="p-10 bg-white rounded-[8px] flex flex-col justify-center items-center">
        <div className="relative w-[154px] h-[36px]">
          <Image
            src="/images/logo.png"
            fill
            alt="logo-image"
            className="object-cover"
            sizes="100"
          />
        </div>
        <div className="w-[480px] flex flex-col gap-[24px] justify-center">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="flex flex-col gap-8"
          >
            <div className="flex flex-col gap-4">
              <Controller
                name="email"
                control={control}
                render={({ field, fieldState: { error } }) => (
                  <TextFiled
                    {...field}
                    errors={error}
                    type="email"
                    id="Логин"
                    label="Логин"
                  />
                )}
              />
              <Controller
                name="password"
                control={control}
                defaultValue=""
                render={({ field, fieldState: { error } }) => (
                  <TextFiled
                    {...field}
                    errors={error}
                    type="password"
                    id="Пароль"
                    label="Пароль"
                  />
                )}
              />
            </div>
            <div className="flex justify-center">
              <button
                type="submit"
                className="border font-railway w-full py-4 px-[64px] rounded-[8px] bg-black text-white"
              >
                Вход
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Login;

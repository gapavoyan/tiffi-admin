"use client";

import { Inter } from "next/font/google";
import "./globals.css";
import Header from "../../components/header/header";
import { usePathname } from "next/navigation";
import { UseLogin } from "../../hooks/auth/use-login";

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const pathname = usePathname();
  const hideHeaderRoutes = ["/login"];

  return (
    <html lang="en">
      <body>
        {!hideHeaderRoutes.includes(pathname) && <Header />}
        {children}
      </body>
    </html>
  );
}

import WithAuth from "../../HOC/with-auth";

function Home() {
  return (
    <WithAuth>
      <p>Welcome</p>
    </WithAuth>
  );
}
export default Home;

"use client";
import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}", // Note the addition of the `app` directory.
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",

    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      fontSize: {
        sm: "12px",
        md: "14px",
        lg: "16px",
        xl: "24px",
      },
      colors: {
        black: "#1B1B1B",
        green: "#168570",
        red: "#F04438",
        white: "#FFFFFF",
      },
      fontFamily: {
        railway: ["Railway", "sans-serif"],
      },
    },
  },
  plugins: [],
};
export default config;

import React from "react";

interface Props {
  setOpen: (open: boolean) => void;
  logout: () => void;
}
function LogoutModal({ setOpen, logout }: Props) {
  return (
    <div className="px-4 py-4 w-max bg-white border flex flex-col items-center gap-2 rounded-[2px] absolute top-20">
      <p className="font-railway">Вы хотите выйти?</p>
      <div className="flex gap-2">
        <button
          className="px-4 py-2 border bg-white rounded-[2px] font-railway"
          onClick={() => setOpen(false)}
        >
          Отменить
        </button>
        <button
          className="px-4 py-2 border bg-black text-white rounded-[2px] font-railway"
          onClick={logout}
        >
          Выйти
        </button>
      </div>
    </div>
  );
}

export default LogoutModal;

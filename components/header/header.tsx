"use client";
import Image from "next/image";
import React, { useState } from "react";
import LogoutModal from "../modals/logout-modal/logout-modal";
import { UseLogin } from "../../hooks/auth/use-login";

function Header() {
  const [open, setOpen] = useState<Boolean>(false);
  const { logout } = UseLogin();
  function handleModalOpen() {
    setOpen(true);
  }
  return (
    <header className="px-[160px] py-[26px]  border-b-[2px] border-black flex justify-between items-center">
      <div>
        <Image
          src="/images/logo.png"
          width={100}
          height={100}
          alt="logoImage"
        />
      </div>
      <div className="flex justify-between items-center gap-4">
        <div className="border-[2px] rounded-full border-green">
          <Image
            src="/images/user.png"
            width={40}
            height={40}
            alt="userImg"
            objectFit="cover"
          />
        </div>
        <p className="font-railway">Имя админа</p>
        <div onClick={handleModalOpen} className="cursor-pointer">
          <Image
            src="/images/logout.svg"
            width={16}
            height={16}
            alt="logoutImg"
          />
        </div>
        {open && <LogoutModal setOpen={setOpen} logout={logout} />}
      </div>
    </header>
  );
}

export default Header;

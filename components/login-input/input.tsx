import React from "react";
import { FieldError } from "react-hook-form";

interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
  errors: FieldError | undefined;
  label: string;
}
function TextFiled({ errors, ...props }: Props) {
  return (
    <div className="flex flex-col gap-2">
      <label htmlFor={props.id} className="font-railway">
        {props.label}
      </label>
      <input
        {...props}
        className="border border-black  py-4 px-4 rounded-[8px]"
      />
      <div className="h-4">
        {errors?.message && <p className="text-red">{errors.message}</p>}
      </div>
    </div>
  );
}

export default TextFiled;

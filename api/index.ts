import ApiSlice from "./slice";
import AuthSlice from "./slices/auth-slice";

export default class Api extends ApiSlice {
  static auth = AuthSlice;
}

import ApiSlice from "../slice";
export default class AuthSlice extends ApiSlice {
  static async PostLogin(info: any) {
    return this.request<{ token: string }>("/admin/auth/login", "POST", info);
  }
}

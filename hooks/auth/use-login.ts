"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";

export function UseLogin() {
  const [token, setToken] = useState<string | null>(
    localStorage.getItem("token")
  );
  const router = useRouter();

  function addToken(token: string) {
    localStorage.setItem("token", token);
    setToken(token);
  }

  function getToken() {
    return token;
  }

  function isAuthenticated() {
    return !!token;
  }
  function logout() {
    localStorage.removeItem("token");
    router.push("/login");
  }
  return {
    addToken,
    getToken,
    isAuthenticated,
    logout,
  };
}

"use client";

import React, { PropsWithChildren, useEffect } from "react";
import { useRouter } from "next/navigation";
import { UseLogin } from "../hooks/auth/use-login";

function withAuth({ children }: PropsWithChildren) {
  const { isAuthenticated } = UseLogin();
  const router = useRouter();

  useEffect(() => {
    if (!isAuthenticated()) {
      router.push("/login");
    }
  }, [isAuthenticated, router]);

  if (!isAuthenticated()) {
    return null;
  }

  return children;
}

export default withAuth;
